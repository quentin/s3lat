# s3lat

## installation

```bash
go get git.deuxfleurs.fr/quentin/s3lat@latest
```

## usage

```bash
export ENDPOINT=[fc00:9a7a:9e::1]:9000
export AWS_ACCESS_KEY_ID=minioadmin
export AWS_SECRET_ACCESS_KEY=minioadmin

s3lat
```

## see also

 - https://git.deuxfleurs.fr/quentin/benchmarks
 - https://git.deuxfleurs.fr/trinity-1686a/mknet
